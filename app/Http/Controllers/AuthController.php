<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar(){
        return view('halaman.form');
    }

    public function submit(Request $request){
        // dd($request->all());
        $nama = $request ['fullname'];
        $alamat = $request['address'];
        return view('halaman.home', compact('nama', 'alamat'));
    }
}
